package com.burgan.cinemaapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.models.moviesModels.IResult
import com.burgan.cinemaapp.models.moviesModels.More
import com.burgan.cinemaapp.models.moviesModels.Result
import kotlinx.android.synthetic.main.row_movies_upcoming.view.*

/**
 * Created by
 * burganbank on 2019-10-17...
 */
class MoviesUpcomingAdapter (private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val movies : MutableList<IResult> = ArrayList()

    private val baseUrl = "https://image.tmdb.org/t/p/original/"

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun getItemViewType(position: Int): Int {
        return when(movies[position]){
            is Result -> 1
            is More -> 0
            else -> throw NotImplementedError()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val movie = movies[position]) {
            is Result -> {
                holder as ViewHolderUpcoming
                Glide.with(context).load(baseUrl + movie.backdrop_path)
                    .into(holder.imageMoviesUpcoming)


                holder.itemView.setOnClickListener {
                }
            }
            is More -> {
                holder as ViewHolderMoreUpcoming

                holder.itemView.setOnClickListener {

                }
            }
            else -> throw NotImplementedError()
        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType) {
            0 -> ViewHolderMoreUpcoming(
                LayoutInflater.from(context).inflate(
                    R.layout.row_movies_more_upcoming,
                    parent,
                    false
                )
            )
            1 -> ViewHolderUpcoming(
                LayoutInflater.from(context).inflate(R.layout.row_movies_upcoming, parent, false)
            )

            else -> throw NotImplementedError()
        }
    }


    fun setItems(moviesToSet: List<IResult>){
        movies.clear()
        movies.addAll(moviesToSet)
        notifyDataSetChanged()
    }

    class ViewHolderUpcoming(view : View) : RecyclerView.ViewHolder (view){
        val imageMoviesUpcoming : ImageView = view.imageMoviesUpcoming
    }

    class ViewHolderMoreUpcoming(view : View ) : RecyclerView.ViewHolder(view)
}
