package com.burgan.cinemaapp.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.burgan.cinemaapp.activities.MoviesPopularDetailActivity
import com.burgan.cinemaapp.activities.MoviesPopularListActivity
import com.burgan.cinemaapp.models.moviesModels.IResult
import com.burgan.cinemaapp.models.moviesModels.More
import com.burgan.cinemaapp.models.moviesModels.Result
import kotlinx.android.synthetic.main.row_movies_popular.view.*
import com.burgan.cinemaapp.R
import android.util.DisplayMetrics






/**
 * Created by
 * burganbank on 2019-10-16...
 */

class MoviesPopularAdapter (private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val movies : MutableList<IResult> = ArrayList()

    private val baseUrl = "https://image.tmdb.org/t/p/w500"


    override fun getItemViewType(position: Int): Int {
        return when(movies[position]) {
            is Result -> 1
            is More -> 0
            else -> throw NotImplementedError()
        }
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder{
        return when(viewType) {
            0 -> ViewHolderMore(
                LayoutInflater.from(
                    context
                ).inflate(R.layout.row_movies_more, parent, false)
            )
            1 -> ViewHolderPopular(
                LayoutInflater.from(
                    context
                ).inflate(R.layout.row_movies_popular, parent, false)
            )
            else -> throw NotImplementedError()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (val movie = movies[position]) {
            is Result -> {
                holder as ViewHolderPopular

                val displayMetrics = DisplayMetrics()
                //val divHeight = displayMetrics.heightPixels
                val divWidth = displayMetrics.widthPixels

                Glide.with(context)
                    .asBitmap()
                    .load(baseUrl + movie.poster_path)
                    .into(object : CustomTarget<Bitmap>(){
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            holder.imageMoviesPopular.setImageBitmap(resource)
                        }
                        override fun onLoadCleared(placeholder: Drawable?) {
                           getSize { width, height ->
                               divWidth * height / width }
                        }
                    })

                holder.titleMoviesPopular.text = movie.title
                holder.txtVoteAverage.text = movie.vote_average.toString()
                holder.itemView.setOnClickListener {
                    val intent = Intent(context, MoviesPopularDetailActivity::class.java)
                    intent.putExtra("movieId",movie.id)
                    context.startActivity(intent)
                }
            }
            is More -> {
                holder as ViewHolderMore
                holder.itemView.setOnClickListener {
                    context.startActivity(Intent(context, MoviesPopularListActivity::class.java))
                }
            }
            else -> throw NotImplementedError()
        }
    }


    class ViewHolderPopular(view: View) : RecyclerView.ViewHolder(view){
        val imageMoviesPopular : ImageView = view.imageMoviesPopular
        val titleMoviesPopular : TextView = view.titleMoviesPopular
        val txtVoteAverage : TextView = view.voteAverage
    }

    class ViewHolderMore(view: View) : RecyclerView.ViewHolder(view)

    fun setItems(moviesToSet: List<IResult>){
        movies.clear()
        movies.addAll(moviesToSet)
        notifyDataSetChanged()
    }
}