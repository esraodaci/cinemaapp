package com.burgan.cinemaapp.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.activities.MoviesNowDetailActivity
import com.burgan.cinemaapp.activities.MoviesNowListActivity
import com.burgan.cinemaapp.models.moviesModels.IResult
import com.burgan.cinemaapp.models.moviesModels.More
import com.burgan.cinemaapp.models.moviesModels.Result
import kotlinx.android.synthetic.main.row_movies_now.*
import kotlinx.android.synthetic.main.row_movies_now.view.*

/**
 * Created by
 * burganbank on 2019-10-15...
 */

class MoviesNowAdapter(private val context: Context) :RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val movies : MutableList<IResult> = ArrayList()

    private val baseUrl = "https://image.tmdb.org/t/p/w500"


    override fun getItemViewType(position: Int): Int {
        return when(movies[position]) {
            is Result -> 1
            is More -> 0
            else -> throw NotImplementedError()
        }
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder{
        return when(viewType) {
            0 -> ViewHolderMore(LayoutInflater.from(context).inflate(R.layout.row_movies_more, parent, false))
            1 -> ViewHolderNow(
                LayoutInflater.from(
                    context
                ).inflate(R.layout.row_movies_now, parent, false)
            )
            else -> throw NotImplementedError()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            val displayMetrics = DisplayMetrics()
            val divWidth = displayMetrics.widthPixels

        when (val movie = movies[position]) {
            is Result -> {
                holder as ViewHolderNow


                Glide.with(context)
                    .asBitmap()
                    .load(baseUrl + movie.poster_path)
                    .into(object : CustomTarget<Bitmap>(){
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            holder.imageMoviesNow.setImageBitmap(resource)

                        }
                        override fun onLoadCleared(placeholder: Drawable?) {
                            getSize { width, height ->
                                divWidth * height / width }
                        }
                    })

                holder.titleMoviesNow.text = movie.title



                holder.itemView.setOnClickListener {
                    val intent = Intent(context, MoviesNowDetailActivity::class.java)
                    intent.putExtra("movieId",movie.id)
                    intent.putExtra("poster_path",movie.poster_path)
                    context.startActivity(intent)
                }
            }
            is More -> {
                holder as ViewHolderMore

                holder.itemView.setOnClickListener {
                    context.startActivity(Intent(context,MoviesNowListActivity::class.java))
                }
            }
            else -> throw NotImplementedError()
        }
    }


    class ViewHolderNow(view: View) : RecyclerView.ViewHolder(view){
        val imageMoviesNow : ImageView = view.imageMoviesNow
        val titleMoviesNow : TextView = view.titleMoviesNow
    }

    class ViewHolderMore(view: View) : RecyclerView.ViewHolder(view)

    fun setItems(moviesToSet: List<IResult>){
        movies.clear()
        movies.addAll(moviesToSet)
        notifyDataSetChanged()
    }
}