package com.burgan.cinemaapp.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.activities.TvPopularDetailActivity
import com.burgan.cinemaapp.models.TvModels.Result
import kotlinx.android.synthetic.main.row_tv_popular.view.*

/**
 * Created by
 * burganbank on 2019-10-25...
 */

class TvPopularAdapter (  private val context : Context) : RecyclerView.Adapter<TvPopularAdapter.ViewHolderTvPopular>() {

    private val tv : MutableList<Result> = ArrayList()
    private val baseUrl = "https://image.tmdb.org/t/p/w500/"


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderTvPopular {
        return ViewHolderTvPopular(LayoutInflater.from(context).inflate(R.layout.row_tv_popular,parent,false))
    }

    override fun getItemCount(): Int {
        return tv.size
    }

    override fun onBindViewHolder(holder: ViewHolderTvPopular, position: Int) {

        val displayMetrics = DisplayMetrics()
        //val divHeight = displayMetrics.heightPixels
        val divWidth = displayMetrics.widthPixels

        Glide.with(context)
            .asBitmap()
            .load(baseUrl + tv[position].poster_path)
            .into(object : CustomTarget<Bitmap>(){
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    holder.imTvPopular.setImageBitmap(resource)
                }
                override fun onLoadCleared(placeholder: Drawable?) {
                    getSize { width, height ->
                        divWidth * height / width }
                }
            })

        //Glide.with(context).load(baseUrl + tv[position].poster_path).into(holder.imTvPopular)
        holder.txtTvPopular.text = tv[position].original_name

        holder.itemView.setOnClickListener {

            val intent = Intent(context, TvPopularDetailActivity::class.java)
            intent.putExtra("tvId",tv[position].id)
            context.startActivity(intent)
        }
    }

    class ViewHolderTvPopular(view : View) : RecyclerView.ViewHolder(view){
        val imTvPopular : ImageView = view.imTvPopular
        val txtTvPopular : TextView = view.txtTvPopular
    }

    fun setItems(tvToSet: List<Result>){
        tv.clear()
        tv.addAll(tvToSet)
        notifyDataSetChanged()
    }
}