package com.burgan.cinemaapp.adapters.pager

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.viewpager.widget.PagerAdapter
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.models.pagerModels.WelcomePagerModel

/**
 * Created by
 * burganbank on 2019-10-09...
 */

class WelcomePA(private val context: Context, private var onNext: (Int) -> Unit) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val modelObject = WelcomePagerModel.values()[position]
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(modelObject.layoutResId, container, false) as ViewGroup
        layout.findViewById<Button>(R.id.buttonNext).setOnClickListener {
            onNext.invoke(position)
        }
        container.addView(layout)
        return layout
    }

    override fun getCount(): Int {
        return WelcomePagerModel.values().size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }


}