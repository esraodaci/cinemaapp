package com.burgan.cinemaapp.adapters.pager

import android.content.Context
import android.view.View
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.PagerAdapter
import com.burgan.cinemaapp.activities.ui.profile.ProfileCommentsFragment
import com.burgan.cinemaapp.activities.ui.profile.ProfileLikeFragment
import com.burgan.cinemaapp.activities.ui.profile.ProfileWatchingFragment

/**
 * Created by
 * burganbank on 2019-10-30...
 */


class ProfilePA : PagerAdapter() {


    private val COUNT = 3

    fun getItem(position: Int) : Fragment?{
        when (position) {
            0 -> {
                return ProfileLikeFragment()
            }
            1 -> {
                return ProfileWatchingFragment()
            }
            2 -> {
                return ProfileCommentsFragment()
            }
            else -> return null
        }
    }

    override fun getCount(): Int {
        return COUNT
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return "Tab " + (position + 1)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }
}
