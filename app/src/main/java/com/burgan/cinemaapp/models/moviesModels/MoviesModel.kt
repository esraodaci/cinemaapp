package com.burgan.cinemaapp.models.moviesModels



/**
 * Created by
 * burganbank on 2019-10-15...
 */


data class MoviesModel(
    val dates: Dates,
    val page: Int,
    val results: List<Result>,
    val total_pages: Int,
    val total_results: Int
)


data class Dates(
    val maximum: String,
    val minimum: String
)


data class Result(
    val adult: Boolean,
    val backdrop_path: String,
    val id: Int,
    val original_language: String,
    val original_title: String,
    val overview: String,
    val popularity: Double,
    val poster_path: String,
    val release_date: String,
    val title: String,
    val video: Boolean,
    val vote_average: Double,
    val vote_count: Int
):IResult


class More: IResult

interface IResult
