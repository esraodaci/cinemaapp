package com.burgan.cinemaapp.models.pagerModels

import com.burgan.cinemaapp.R

/**
 * Created by
 * burganbank on 2019-10-10...
 */
enum class WelcomePagerModel constructor(val layoutResId : Int){

    FIRST(R.layout.pager_first_page),
    SECOND(R.layout.pager_second_page),
    THIRD(R.layout.pager_third_page)
}