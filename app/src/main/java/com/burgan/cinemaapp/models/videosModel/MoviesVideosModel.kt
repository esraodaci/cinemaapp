package com.burgan.cinemaapp.models.videosModel

/**
 * Created by
 * burganbank on 2019-10-21...
 */
data class MoviesVideosModel(
    val id: Int,
    val results: List<Result>
)

data class Result(
    val id: String,
    val iso_3166_1: String,
    val iso_639_1: String,
    val key: String,
    val name: String,
    val site: String,
    val size: Int,
    val type: String
)
