package com.burgan.cinemaapp.viewModels

import androidx.lifecycle.ViewModel
import com.burgan.cinemaapp.models.TvModels.Result
import com.burgan.cinemaapp.models.TvModels.TvPopularDetailModel
import com.burgan.cinemaapp.repository.TvRepo

/**
 * Created by
 * burganbank on 2019-10-25...
 */
class TvVM : ViewModel(){
    val repo =  TvRepo()

    fun getObjectsTvPOpularAsync(onResponse: (resp : List<Result>) -> Unit){
        return repo.getObjectsTvPopularAsync(onResponse)
    }

    fun getTvPoPularDetailAsync(tvId : String,onResponse: (resp : TvPopularDetailModel)->Unit){
        return repo.getTvPoPularDetailAsync(tvId,onResponse)
    }
}
