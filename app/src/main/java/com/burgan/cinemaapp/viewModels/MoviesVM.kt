package com.burgan.cinemaapp.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.burgan.cinemaapp.models.moviesModels.MoviesDetailModel
import com.burgan.cinemaapp.models.videosModel.MoviesYoutubeVideoModel
import com.burgan.cinemaapp.models.videosModel.Result
import com.burgan.cinemaapp.repository.MoviesRepo

/**
 * Created by
 * burganbank on 2019-10-15...
 */
class MoviesVM (application: Application) : AndroidViewModel(application){

    private val repository = MoviesRepo()


    fun getObjectsNowAsync( onResponse : (resp : List<com.burgan.cinemaapp.models.moviesModels.Result>) -> Unit){
        return repository.getObjectsNowAsync(onResponse)
    }

    fun getObjectsPopularAsync( onResponse : (resp : List<com.burgan.cinemaapp.models.moviesModels.Result>) -> Unit){
        return repository.getObjectsPopularAsync(onResponse)
    }

    fun getObjectsUpcomingAsync(onResponse: (resp: List<com.burgan.cinemaapp.models.moviesModels.Result>) -> Unit){
        return repository.getObjectsUpcomingAsync(onResponse)
    }

    fun getObjectsVideos(movieId : String,onResponse: (resp: List<Result>)->Unit){
        return repository.getObjectsVideos(movieId,onResponse)
    }

    fun getObjectsDetail(movieId : String,onResponse: (resp: MoviesDetailModel) -> Unit){
        return repository.getObjectsDetail(movieId,onResponse)
    }

    fun getObjectsYoutubeVideo(movieId : String,onResponse: (resp: MoviesYoutubeVideoModel) -> Unit){
        return repository.getObjectsYoutubeVideo(movieId,onResponse)
    }
}
