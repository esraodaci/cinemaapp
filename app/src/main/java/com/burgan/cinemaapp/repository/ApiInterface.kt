package com.burgan.cinemaapp.repository

import com.burgan.cinemaapp.models.TvModels.TvPopularDetailModel
import com.burgan.cinemaapp.models.TvModels.TvPopularModel
import com.burgan.cinemaapp.models.moviesModels.MoviesDetailModel
import com.burgan.cinemaapp.models.moviesModels.MoviesModel
import com.burgan.cinemaapp.models.videosModel.MoviesVideosModel
import com.burgan.cinemaapp.models.videosModel.MoviesYoutubeVideoModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap
import retrofit2.http.Url

/**
 * Created by
 * burganbank on 2019-10-11...
 */
interface ApiInterface {

    /* MOVIES */

    @GET("movie/now_playing?api_key=0678fdcff2992b79fe3ab007e4c64834&language=tr-TR&page=1&region=TR")
    fun getNowPlaying(): Call<MoviesModel>

    @GET("movie/popular?api_key=0678fdcff2992b79fe3ab007e4c64834&language=en-US&page=1")
    fun getPopular() : Call<MoviesModel>

    @GET("movie/upcoming?api_key=0678fdcff2992b79fe3ab007e4c64834&language=en-US&page=1")
    fun getUpcoming() : Call<MoviesModel>

    @GET
    fun getVideos(@Url url: String ) : Call<MoviesVideosModel>

    @GET
    fun getDetail(@Url url: String ) : Call<MoviesDetailModel>

    @GET
    fun getYoutubeVideo(@Url url: String) : Call<MoviesYoutubeVideoModel>


    /* TV */

    @GET
    fun getTvPopular(@Url url: String) : Call<TvPopularModel>

    @GET
    fun getTvPopularDeteil(@Url url: String) : Call<TvPopularDetailModel>
}