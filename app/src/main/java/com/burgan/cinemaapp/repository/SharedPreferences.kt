package com.burgan.cinemaapp.repository

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by
 * burganbank on 2019-10-10...
 */
class SharedPreferences(val context: Context) {

        private val sharedPreference: SharedPreferences = context.getSharedPreferences("Welcome", Context.MODE_PRIVATE)

        fun save(value : String ){

            var editor = sharedPreference.edit()
            editor.putString("firstLogin",value )

            return editor.apply()
        }

        fun getValueString(): String? {

            return sharedPreference.getString("firstLogin", "true")
        }

}
