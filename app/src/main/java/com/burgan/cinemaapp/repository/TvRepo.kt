package com.burgan.cinemaapp.repository

import com.burgan.cinemaapp.models.TvModels.Result
import com.burgan.cinemaapp.models.TvModels.TvPopularDetailModel
import com.burgan.cinemaapp.models.TvModels.TvPopularModel
import com.burgan.cinemaapp.repository.urls.ConnectUrls
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by
 * burganbank on 2019-10-25...
 */
class TvRepo {

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.themoviedb.org/3/")
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()
        .create(ApiInterface::class.java)

    fun getObjectsTvPopularAsync(onResponse: (resp : List<Result>) -> Unit){

        val tvApi = retrofit.getTvPopular(ConnectUrls.getTvPopular()).enqueue( object : Callback<TvPopularModel>{
            override fun onFailure(call: Call<TvPopularModel>, t: Throwable) {
            }

            override fun onResponse(call: Call<TvPopularModel>, response: Response<TvPopularModel>) {
                val response = response.body()!!.results
                onResponse.invoke(response)
            }
        })
    }

    fun getTvPoPularDetailAsync(tvId : String, onResponse: (resp : TvPopularDetailModel )->Unit){

        val tvApi = retrofit.getTvPopularDeteil(ConnectUrls.getTvPopularDetail(tvId)).enqueue(object : Callback<TvPopularDetailModel>{
            override fun onFailure(call: Call<TvPopularDetailModel>, t: Throwable) {
            }

            override fun onResponse(call: Call<TvPopularDetailModel>, response: Response<TvPopularDetailModel>) {
                val response = response.body()!!
                onResponse.invoke(response)
            }
        })

    }


}

