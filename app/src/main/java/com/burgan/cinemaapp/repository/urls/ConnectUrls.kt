package com.burgan.cinemaapp.repository.urls

/**
 * Created by
 * burganbank on 2019-10-21...
 */
object ConnectUrls {

    /*Movies */
    private const val api_get_now_playing = "movie/now_playing?api_key=0678fdcff2992b79fe3ab007e4c64834&language=tr-TR&page=1&region=TR"
    private const val api_get_popular = "movie/popular?api_key=0678fdcff2992b79fe3ab007e4c64834&language=en-US&page=1"
    private const val api_get_upcoming = "movie/upcoming?api_key=0678fdcff2992b79fe3ab007e4c64834&language=en-US&page=1"
    private const val api_get_videos = "movie/{movieId}/videos?api_key=0678fdcff2992b79fe3ab007e4c64834&language=en-US"
    private const val api_get_detail = "movie/{movieId}?api_key=0678fdcff2992b79fe3ab007e4c64834&language=en-US"
    private const val api_get_youtube_video = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id={movieId}&key=AIzaSyBjOyf6jnLQjS_6brl7h8O4pc2slhrtzgA"

    private const val path_movieId = "{movieId}"

    /* TV */

    private const val api_get_tv_popular = "tv/popular?api_key=0678fdcff2992b79fe3ab007e4c64834&language=en-US&page=1"
    private const val api_get_tv_popular_detail = "tv/{tvId}?api_key=0678fdcff2992b79fe3ab007e4c64834&language=en-US"

    private const val path_tvId = "{tvId}"


    //MOVIE

    fun getNowPlaying(): String {
        return api_get_now_playing
    }

    fun getPopular(): String{
        return api_get_popular
    }

    fun getUpcoming(): String{
        return api_get_upcoming
    }

    fun getVideos(movieId : String) : String{
        return api_get_videos.replace(path_movieId,movieId)
    }

    fun getDetail(movieId: String) : String{
        return api_get_detail.replace(path_movieId,movieId)
    }

    fun getYotubeVideo(movieId: String) : String{
        return api_get_youtube_video.replace(path_movieId,movieId)
    }

    //TV

    fun getTvPopular() : String{
        return api_get_tv_popular
    }

    fun getTvPopularDetail(tvId : String) : String {
        return api_get_tv_popular_detail.replace(path_tvId,tvId)
    }

}
