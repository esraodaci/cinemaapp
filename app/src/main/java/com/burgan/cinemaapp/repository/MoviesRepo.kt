package com.burgan.cinemaapp.repository

import com.burgan.cinemaapp.models.moviesModels.MoviesDetailModel
import com.burgan.cinemaapp.models.moviesModels.Result
import com.burgan.cinemaapp.models.moviesModels.MoviesModel
import com.burgan.cinemaapp.models.videosModel.MoviesVideosModel
import com.burgan.cinemaapp.models.videosModel.MoviesYoutubeVideoModel
import com.burgan.cinemaapp.repository.urls.ConnectUrls
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by
 * burganbank on 2019-10-15...
 */

class MoviesRepo(){

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://api.themoviedb.org/3/")
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()
        .create(ApiInterface::class.java)





    fun getObjectsNowAsync(onResponse: (resp : List<Result>) -> Unit) {

        val moviesApi = retrofit.getNowPlaying().enqueue(object : Callback<MoviesModel> {

            override fun onFailure(call: Call<MoviesModel>, t: Throwable) {
            }

            override fun onResponse(call: Call<MoviesModel>, response: Response<MoviesModel>) {
                val response = response.body()!!.results
                onResponse.invoke(response)
            }
        })
    }

    fun getObjectsPopularAsync(onResponse: (resp : List<Result>) -> Unit){

        val moviesApi = retrofit.getPopular().enqueue(object : Callback<MoviesModel> {

            override fun onFailure(call: Call<MoviesModel>, t: Throwable) {
            }

            override fun onResponse(call: Call<MoviesModel>, response: Response<MoviesModel>) {
                val response = response.body()!!.results
                onResponse.invoke(response)
            }
        })

    }

    fun getObjectsUpcomingAsync(onResponse: (resp: List<Result>) -> Unit){
        val moviesApi = retrofit.getUpcoming().enqueue(object : Callback<MoviesModel>{
            override fun onFailure(call: Call<MoviesModel>, t: Throwable) {
            }

            override fun onResponse(call: Call<MoviesModel>, response: Response<MoviesModel>) {
            val response = response.body()!!.results
                onResponse.invoke(response)
            }
        })
    }

    fun getObjectsVideos(movieId : String,onResponse: (resp: List<com.burgan.cinemaapp.models.videosModel.Result>) -> Unit){

        val moviesApi = retrofit.getVideos(ConnectUrls.getVideos(movieId)).enqueue( object : Callback<MoviesVideosModel>{
            override fun onFailure(call: Call<MoviesVideosModel>, t: Throwable) {
            }

            override fun onResponse(call: Call<MoviesVideosModel>, response: Response<MoviesVideosModel>) {
                val response = response.body()!!.results
                onResponse.invoke(response)
            }
        })

    }

    fun getObjectsDetail(movieId : String,onResponse: (resp: MoviesDetailModel) -> Unit){

        val moviesApi = retrofit.getDetail(ConnectUrls.getDetail(movieId)).enqueue( object : Callback<MoviesDetailModel>{
            override fun onFailure(call: Call<MoviesDetailModel>, t: Throwable) {
            }

            override fun onResponse(call: Call<MoviesDetailModel>, response: Response<MoviesDetailModel>) {
                val response = response.body()!!
                onResponse.invoke(response)
            }
        })

    }

    fun getObjectsYoutubeVideo(movieId : String,onResponse: (resp: MoviesYoutubeVideoModel) -> Unit){
        val api = retrofit.getYoutubeVideo(ConnectUrls.getYotubeVideo(movieId)).enqueue( object : Callback<MoviesYoutubeVideoModel>{
            override fun onFailure(call: Call<MoviesYoutubeVideoModel>, t: Throwable) {
            }

            override fun onResponse(call: Call<MoviesYoutubeVideoModel>, response: Response<MoviesYoutubeVideoModel>) {
                val response = response.body()!!
                onResponse.invoke(response)
            }
        })
    }
}
