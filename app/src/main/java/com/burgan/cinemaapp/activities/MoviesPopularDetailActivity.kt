package com.burgan.cinemaapp.activities

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.models.videosModel.Result
import com.burgan.cinemaapp.viewModels.MoviesVM
import kotlinx.android.synthetic.main.activity_movies_popular_detail.*

class MoviesPopularDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies_popular_detail)
        setSupportActionBar(toolbar)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        val moviesId = intent.getIntExtra("movieId",0)

        MoviesVM(application).getObjectsVideos(movieId = moviesId.toString(),onResponse = {
                resp ->
            val itemsNow: MutableList<Result> = ArrayList()
            itemsNow.addAll(resp)

        })
    }
}
