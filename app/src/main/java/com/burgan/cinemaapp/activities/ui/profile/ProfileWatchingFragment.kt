package com.burgan.cinemaapp.activities.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.burgan.cinemaapp.R

/**
 * Created by
 * burganbank on 2019-10-30...
 */
class ProfileWatchingFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_profile_watching, container, false)

        return view
    }
}