package com.burgan.cinemaapp.activities.ui.movies

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.adapters.MoviesNowAdapter
import com.burgan.cinemaapp.adapters.MoviesPopularAdapter
import com.burgan.cinemaapp.adapters.MoviesUpcomingAdapter
import com.burgan.cinemaapp.models.moviesModels.IResult
import com.burgan.cinemaapp.models.moviesModels.More
import com.burgan.cinemaapp.viewModels.MoviesVM
import kotlinx.android.synthetic.main.fragment_movies.*

class MoviesFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


      //NOW PLAYING
        val nowAdapter = MoviesNowAdapter(context!!)
        rvNow.adapter = nowAdapter
        rvNow.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvNow.setHasFixedSize(true)
        MoviesVM(application = Application()).getObjectsNowAsync{ resp ->

            val itemsNow: MutableList<IResult> = ArrayList()
            itemsNow.addAll(resp)
            itemsNow.add(More())
            nowAdapter.setItems(itemsNow)
        }

        //POPULAR
        val popularAdapter = MoviesPopularAdapter(context!!)
        rvPopular.adapter = popularAdapter
        rvPopular.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvPopular.setHasFixedSize(true)
        MoviesVM(application = Application()).getObjectsPopularAsync{ resp ->
            val itemsPopular: MutableList<IResult> = ArrayList()
            itemsPopular.addAll(resp)
            itemsPopular.add(More())
            popularAdapter.setItems(itemsPopular)
        }

        //UPCOMING

        val upcomingAdapter = MoviesUpcomingAdapter(context!!)
        rvUpcoming.adapter = upcomingAdapter
        rvUpcoming.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvUpcoming.setHasFixedSize(true)
        MoviesVM(application = Application()).getObjectsUpcomingAsync { resp ->
            val itemsUpcoming : MutableList<IResult> = ArrayList()
            itemsUpcoming.add(More())
            itemsUpcoming.addAll(resp)
            upcomingAdapter.setItems(itemsUpcoming)

        }




    }
}