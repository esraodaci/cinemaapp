package com.burgan.cinemaapp.activities

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.adapters.MoviesPopularAdapter
import com.burgan.cinemaapp.models.moviesModels.IResult
import com.burgan.cinemaapp.viewModels.MoviesVM
import kotlinx.android.synthetic.main.activity_movies_popular_list.*

class MoviesPopularListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies_popular_list)

        //NOW PLAYING
        val popularListAdapter = MoviesPopularAdapter(this)
        rvPopularList.adapter = popularListAdapter
        rvPopularList.layoutManager = GridLayoutManager(this,2)
        rvPopularList.setHasFixedSize(true)

        MoviesVM(application = Application()).getObjectsNowAsync { resp ->

            val itemsNow: MutableList<IResult> = ArrayList()
            itemsNow.addAll(resp)
            popularListAdapter.setItems(itemsNow)
        }
    }
}
