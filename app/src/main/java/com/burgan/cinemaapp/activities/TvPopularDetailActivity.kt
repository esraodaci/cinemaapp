package com.burgan.cinemaapp.activities

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.models.TvModels.TvPopularDetailModel
import com.burgan.cinemaapp.viewModels.TvVM
import kotlinx.android.synthetic.main.activity_tv_popular_detail.*




class TvPopularDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tv_popular_detail)
        setSupportActionBar(toolbar)

        val tvId = intent.getIntExtra("tvId",0).toString()
        var tvItems : MutableList<TvPopularDetailModel> = ArrayList()


        TvVM().getTvPoPularDetailAsync(tvId,onResponse = { resp ->
            tvItems.addAll(listOf(resp))
        })

        Glide.with(this)
            .load("https://image.tmdb.org/t/p/w500" + tvItems[0].poster_path)
            .into(imTvPopularDetail)
    }
}
