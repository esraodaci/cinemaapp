package com.burgan.cinemaapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.adapters.pager.WelcomePA
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_welcome)

            val pagerAdapter =
                WelcomePA(this, onNext = { position ->
                    if (position != 2) {
                        viewPager.currentItem = position + 1
                    } else {
                        val intent = Intent(this, MoviesBottomNavigationActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                })
            viewPager.adapter = pagerAdapter
    }
}
