package com.burgan.cinemaapp.activities

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.GridLayoutManager
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.adapters.MoviesNowAdapter
import com.burgan.cinemaapp.models.moviesModels.Result
import com.burgan.cinemaapp.viewModels.MoviesVM
import kotlinx.android.synthetic.main.activity_movies_now_list.*
import kotlinx.android.synthetic.main.row_movies_now.*

class MoviesNowListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies_now_list)

        //NOW PLAYING
        val nowListAdapter = MoviesNowAdapter(this)
        rvNowList.adapter = nowListAdapter
        rvNowList.layoutManager = GridLayoutManager(this,2)!!
        rvNowList.setHasFixedSize(true)

        MoviesVM(application = Application()).getObjectsNowAsync { resp ->

            val itemsNow: MutableList<Result> = ArrayList()
            itemsNow.addAll(resp)
            nowListAdapter.setItems(itemsNow)
        }

    }
}
