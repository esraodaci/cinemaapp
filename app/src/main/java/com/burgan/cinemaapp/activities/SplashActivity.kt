package com.burgan.cinemaapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.repository.SharedPreferences

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //hiding title bar of this activity
        window.requestFeature(Window.FEATURE_NO_TITLE)

        //making this activity full screen
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_splash)

        Handler().postDelayed({

            if(SharedPreferences(this@SplashActivity).getValueString() == "true"){
                SharedPreferences(this@SplashActivity).save("false")
                startActivity(Intent(this@SplashActivity, WelcomeActivity::class.java))
                finish()
            }else{
                startActivity(Intent(this@SplashActivity, MoviesBottomNavigationActivity::class.java))
                finish()
            }

        }, 1000)




    }
}
