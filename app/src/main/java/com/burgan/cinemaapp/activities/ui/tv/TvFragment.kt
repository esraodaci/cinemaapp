package com.burgan.cinemaapp.activities.ui.tv

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.burgan.cinemaapp.R
import com.burgan.cinemaapp.adapters.TvPopularAdapter
import com.burgan.cinemaapp.models.TvModels.Result
import com.burgan.cinemaapp.viewModels.TvVM
import kotlinx.android.synthetic.main.fragment_tv.*

class TvFragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_tv, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val tvAdapter =  TvPopularAdapter(context!!)
        rvTvPopular.adapter = tvAdapter
        rvTvPopular.layoutManager = GridLayoutManager(context,2)!!
        rvTvPopular.setHasFixedSize(true)



        TvVM().getObjectsTvPOpularAsync {resp ->
            val itemsTv: MutableList<Result> = ArrayList()
            itemsTv.addAll(resp)
            tvAdapter.setItems(itemsTv)
        }
    }
}