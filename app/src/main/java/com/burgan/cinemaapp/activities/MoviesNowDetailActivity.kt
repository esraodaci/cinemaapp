package com.burgan.cinemaapp.activities

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import at.huber.youtubeExtractor.YouTubeExtractor
import com.burgan.cinemaapp.models.videosModel.Result
import com.burgan.cinemaapp.viewModels.MoviesVM
import kotlinx.android.synthetic.main.activity_movies_now_detail.*
import retrofit2.http.Url
import vimeoextractor.OnVimeoExtractionListener
import vimeoextractor.VimeoExtractor
import vimeoextractor.VimeoVideo


class MoviesNowDetailActivity : AppCompatActivity() {

    val itemsNow: MutableList<Result> = ArrayList()
    private var mediaController: android.widget.MediaController? = null
    private var TAG = "VideoPlayer"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.burgan.cinemaapp.R.layout.activity_movies_now_detail)

        val moviesId = intent.getIntExtra("movieId",0)

        /*val posterPath = intent.getStringExtra("poster_path")
        Glide.with(this)
            .load("https://image.tmdb.org/t/p/w500$posterPath")
            .into(imNowDetailPoster)*/

        MoviesVM(application).getObjectsVideos(movieId = moviesId.toString(), onResponse = { resp ->
                itemsNow.addAll(resp)

                mediaController = android.widget.MediaController(this)
                videoNow.setMediaController(mediaController)
            var path = "https://www.youtube.com/get_video_info?html5=1&video_id=${itemsNow[0].key}"

            initializePlayer(path)

            videoNow.setOnPreparedListener { mp ->
                    mp.isLooping = true
                    Log.i(TAG, "Duration = " + videoNow.duration)
                }

            })

    }

    private fun initializePlayer(path : String) {
        YouTubeExtractor.getInstance().fetchVideoWithURL(path, null, object :
            OnVimeoExtractionListener {
            override fun onSuccess(video: VimeoVideo) {
                val videoStream = video.streams["720p"]
                videoStream?.let{
                    playVideo(videoStream)
                }
            }

            override fun onFailure(throwable: Throwable) {
            }
        })
    }

    fun playVideo(videoStream: String) {
        runOnUiThread {
           videoNow.setVideoPath(videoStream)
            videoNow.requestFocus()
            videoNow.setOnPreparedListener { mp ->
                mp.isLooping = true
                videoNow.start()
            }
        }
    }
}
